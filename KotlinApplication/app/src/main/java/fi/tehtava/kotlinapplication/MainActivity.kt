package fi.tehtava.kotlinapplication

import android.content.res.Resources
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import fi.tehtava.kotlinapplication.Utils.Utils.onActivityCreateSetTheme
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val TAG = "Utils"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Utils.Utils.onActivityCreateSetTheme(this)
        setContentView(R.layout.activity_main)

        // get reference to items
        val button1 = findViewById(R.id.button1) as Button
        val button2 = findViewById(R.id.button2) as Button
        val button3 = findViewById(R.id.button3) as Button
        val button4 = findViewById(R.id.button4) as Button
        val backGround = findViewById(R.id.backGround) as View
        val textView = findViewById(R.id.textView) as TextView

        colorChanger(this, CurrentPosition.CurrentPosition.currentPosition)

        // set on-click listeners
        button1.setOnClickListener {
            Toast.makeText(this@MainActivity, "You clicked button 1.", Toast.LENGTH_SHORT).show()
            CurrentPosition.CurrentPosition.currentPosition = 0
            backGround.setBackgroundColor(resources.getColor(R.color.colorBackground1))
            button1.setBackgroundResource(R.drawable.button)
            button2.setBackgroundResource(R.drawable.button)
            button3.setBackgroundResource(R.drawable.button)
            button4.setBackgroundResource(R.drawable.button)
            textView.setTextColor(resources.getColor(R.color.colorText))
            Utils.Utils.changeToTheme(this, 0)
        }
        button2.setOnClickListener {
            Toast.makeText(this@MainActivity, "You clicked button 2.", Toast.LENGTH_SHORT).show()
            CurrentPosition.CurrentPosition.currentPosition = 1
            backGround.setBackgroundColor(resources.getColor(R.color.colorBackground2))
            button1.setBackgroundResource(R.drawable.button2)
            button2.setBackgroundResource(R.drawable.button2)
            button3.setBackgroundResource(R.drawable.button2)
            button4.setBackgroundResource(R.drawable.button2)
            textView.setTextColor(resources.getColor(R.color.colorText2))
            Utils.Utils.changeToTheme(this, 1)
        }
        button3.setOnClickListener {
            Toast.makeText(this@MainActivity, "You clicked button 3.", Toast.LENGTH_SHORT).show()
            CurrentPosition.CurrentPosition.currentPosition = 2
            backGround.setBackgroundColor(resources.getColor(R.color.colorBackground3))
            button1.setBackgroundResource(R.drawable.button3)
            button2.setBackgroundResource(R.drawable.button3)
            button3.setBackgroundResource(R.drawable.button3)
            button4.setBackgroundResource(R.drawable.button3)
            textView.setTextColor(resources.getColor(R.color.colorText3))
            Utils.Utils.changeToTheme(this, 2)
        }
        button4.setOnClickListener {
            Toast.makeText(this@MainActivity, "You clicked button 4.", Toast.LENGTH_SHORT).show()
            CurrentPosition.CurrentPosition.currentPosition = 3
            backGround.setBackgroundColor(resources.getColor(R.color.colorBackground4))
            button1.setBackgroundResource(R.drawable.button4)
            button2.setBackgroundResource(R.drawable.button4)
            button3.setBackgroundResource(R.drawable.button4)
            button4.setBackgroundResource(R.drawable.button4)
            textView.setTextColor(resources.getColor(R.color.colorText4))
            Utils.Utils.changeToTheme(this, 3)
        }
    }

    override fun onClick(v: View) {
        TODO("Not yet implemented")
    }

    fun colorChanger(activity: MainActivity, position: Int) {
        when (position) {
            0 -> {
                backGround.setBackgroundColor(resources.getColor(R.color.colorBackground1))
                button1.setBackgroundResource(R.drawable.button)
                button2.setBackgroundResource(R.drawable.button)
                button3.setBackgroundResource(R.drawable.button)
                button4.setBackgroundResource(R.drawable.button)
                textView.setTextColor(resources.getColor(R.color.colorText))
            }
            1 -> {
                backGround.setBackgroundColor(resources.getColor(R.color.colorBackground2))
                button1.setBackgroundResource(R.drawable.button2)
                button2.setBackgroundResource(R.drawable.button2)
                button3.setBackgroundResource(R.drawable.button2)
                button4.setBackgroundResource(R.drawable.button2)
                textView.setTextColor(resources.getColor(R.color.colorText2))
            }
            2 -> {
                backGround.setBackgroundColor(resources.getColor(R.color.colorBackground3))
                button1.setBackgroundResource(R.drawable.button3)
                button2.setBackgroundResource(R.drawable.button3)
                button3.setBackgroundResource(R.drawable.button3)
                button4.setBackgroundResource(R.drawable.button3)
                textView.setTextColor(resources.getColor(R.color.colorText3))
            }
            3 -> {
                backGround.setBackgroundColor(resources.getColor(R.color.colorBackground4))
                button1.setBackgroundResource(R.drawable.button4)
                button2.setBackgroundResource(R.drawable.button4)
                button3.setBackgroundResource(R.drawable.button4)
                button4.setBackgroundResource(R.drawable.button4)
                textView.setTextColor(resources.getColor(R.color.colorText4))
            }
        }
    }
}