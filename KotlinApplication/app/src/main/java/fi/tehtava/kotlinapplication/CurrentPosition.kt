package fi.tehtava.kotlinapplication

import android.app.Application

class CurrentPosition : Application() {
    object CurrentPosition {
        // app level variable to retain selected "button" value
        var currentPosition = 0
    }
}