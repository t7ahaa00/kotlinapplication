package fi.tehtava.kotlinapplication

import android.R

import android.app.Activity

import android.content.Intent
import android.util.Log
import android.util.Log.d
import android.view.View
import android.widget.Button
import android.widget.TextView


class Utils {
    object Utils {

        private const val TAG = "Utils"

        private var sTheme = 0
        private const val THEME_BLUE = 0
        private const val THEME_PINK = 1
        private const val THEME_GREEN = 2
        private const val THEME_RED = 3

        // get reference to items
        private var button1: Button? = null
        private var button2: Button? = null
        private var button3: Button? = null
        private var button4: Button? = null
        private var backGround: View? = null
        private var textView: TextView? = null


        fun changeToTheme(activity: Activity, theme: Int) {
            sTheme = theme

            button1 = activity.findViewById(fi.tehtava.kotlinapplication.R.id.button1) as Button
            button2 = activity.findViewById(fi.tehtava.kotlinapplication.R.id.button1) as Button
            button3 = activity.findViewById(fi.tehtava.kotlinapplication.R.id.button1) as Button
            button4 = activity.findViewById(fi.tehtava.kotlinapplication.R.id.button1) as Button
            backGround = activity.findViewById(fi.tehtava.kotlinapplication.R.id.backGround) as View
            textView = activity.findViewById(fi.tehtava.kotlinapplication.R.id.textView) as TextView
            Log.i(TAG, "theme")
            activity.finish()
            activity.startActivity(Intent(activity, activity.javaClass))
            activity.overridePendingTransition(
                R.anim.fade_in,
                R.anim.fade_out
            )
        }

        fun onActivityCreateSetTheme(activity: Activity) {
            when (sTheme) {
                THEME_BLUE -> {

                    backGround?.setBackgroundColor(activity.resources.getColor(fi.tehtava.kotlinapplication.R.color.colorBackground1))
                    button1?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button)
                    button2?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button)
                    button3?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button)
                    button4?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button)
                    textView?.setTextColor(activity.resources.getColor(fi.tehtava.kotlinapplication.R.color.colorText))
                    activity.setTheme(fi.tehtava.kotlinapplication.R.style.AppThemeBlue)
                }
                THEME_PINK -> {

                    backGround?.setBackgroundColor(activity.resources.getColor(fi.tehtava.kotlinapplication.R.color.colorBackground2))
                    button1?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button2)
                    button2?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button2)
                    button3?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button2)
                    button4?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button2)
                    textView?.setTextColor(activity.resources.getColor(fi.tehtava.kotlinapplication.R.color.colorText2))
                    activity.setTheme(fi.tehtava.kotlinapplication.R.style.AppThemePink)
                }
                THEME_GREEN -> {

                    backGround?.setBackgroundColor(activity.resources.getColor(fi.tehtava.kotlinapplication.R.color.colorBackground3))
                    button1?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button3)
                    button2?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button3)
                    button3?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button3)
                    button4?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button3)
                    textView?.setTextColor(activity.resources.getColor(fi.tehtava.kotlinapplication.R.color.colorText3))
                    activity.setTheme(fi.tehtava.kotlinapplication.R.style.AppThemeGreen)
                }
                THEME_RED -> {

                    backGround?.setBackgroundColor(activity.resources.getColor(fi.tehtava.kotlinapplication.R.color.colorBackground4))
                    button1?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button4)
                    button2?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button4)
                    button3?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button4)
                    button4?.setBackgroundResource(fi.tehtava.kotlinapplication.R.drawable.button4)
                    textView?.setTextColor(activity.resources.getColor(fi.tehtava.kotlinapplication.R.color.colorText4))
                    activity.setTheme(fi.tehtava.kotlinapplication.R.style.AppThemeRed)
                }
                else -> {
                    activity.setTheme(fi.tehtava.kotlinapplication.R.style.AppThemeBlue)
                }
            }
        }
    }
}